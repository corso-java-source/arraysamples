package it.stefanobizzi.corsi.javase.arraysamples;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayAdvancedChallenge {
    public static void main(String[] args) {
        short[][] toBeOrdered = {
                {12, 21, 87, 38},
                {12, 7, 25, 96},
                {12, 14, 68, 42},
                {12, 1, 65, 25}
        };

        for(short[] element: toBeOrdered) {
            Arrays.sort(element);
            // System.out.println(Arrays.toString(element));
        }

        bubbleSort(toBeOrdered);
        for (short[] element: toBeOrdered) {
            System.out.println(Arrays.toString(element));
        }

        Scanner scanner = new Scanner(System.in);
        System.out.print("Insert a number: ");
        short needle = scanner.nextShort();
        do {
            for(int i = 0; i < toBeOrdered.length; i++) {
                int found = Arrays.binarySearch(toBeOrdered[i], needle);
                if (found >= 0) {
                    System.out.println("Element found at index " + found + " of array with index " + i);
                } else {
                    System.out.println("Element not found in array " + i);
                }
            }
            System.out.print("Insert a number: ");
            needle = scanner.nextShort();
        } while (needle != -1);
    }

    public static void bubbleSort(short[][] array) {
        boolean elementsAreOrdered;

        do {
            elementsAreOrdered = true;
            for(int i = 0; i < array.length - 1; i++) {
                int res = Arrays.compare(array[i], array[i+1]);
                if (res > 0) {
                    short[] temp;
                    temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    elementsAreOrdered = false;
                }
            }
        } while (!elementsAreOrdered);
    }
}

























