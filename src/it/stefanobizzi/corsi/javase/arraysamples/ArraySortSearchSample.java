package it.stefanobizzi.corsi.javase.arraysamples;

import java.util.Arrays;

public class ArraySortSearchSample {
    public static void main(String[] args) {
        String[] names = {"Elisa", "Alberto", "Carlo", "Francesco", "Beatrice", "Giuseppe", "Daniele"};
        Arrays.sort(names, 3, 6);
        System.out.println(Arrays.toString(names));
        Arrays.sort(names);
        System.out.println(Arrays.toString(names));

        System.out.print("Francesco: ");
        System.out.println(Arrays.binarySearch(names, "Francesco"));

        System.out.print("Francesco (0-3): ");
        System.out.println(Arrays.binarySearch(names, 0, 3, "Francesco"));

        System.out.print("Ada: ");
        System.out.println(Arrays.binarySearch(names, "Ada"));

        System.out.print("Alfio: ");
        System.out.println(Arrays.binarySearch(names, "Alfio"));
    }
}
