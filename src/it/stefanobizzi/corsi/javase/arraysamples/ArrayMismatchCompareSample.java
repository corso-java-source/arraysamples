package it.stefanobizzi.corsi.javase.arraysamples;

import java.util.Arrays;

public class ArrayMismatchCompareSample {
    public static void main(String[] args) {
        short[] shortsOne = {1, 2, 3, 4};
        short[] shortsTwo = {1, 2, 8, 9};
        short[] shortsThree = {1, 2, 8 ,9};

        System.out.println(Arrays.mismatch(shortsOne, shortsTwo));
        System.out.println(Arrays.mismatch(shortsTwo, shortsThree));

        System.out.println(Arrays.compare(shortsOne, shortsTwo));
        System.out.println(Arrays.compare(shortsTwo, shortsOne));
        System.out.println(Arrays.compare(shortsTwo, shortsThree));

        int[] intsOne = {1, 2, 3, 4};
        int[] intsTwo = {1, 2, 8, 9};
        System.out.println(Arrays.compare(intsOne, intsTwo));
    }
}
