package it.stefanobizzi.corsi.javase.arraysamples;

public class StudentChallenge {
    public static void main(String[] args) {
        String[] students = {"Alberto", "Beatrice", "Carlo", "Daniele", "Elisa"};
        byte[][] grades = new byte[students.length][];
        for (int studentIndex = 0; studentIndex < students.length; studentIndex++) {
            System.out.print("Grades for " + students[studentIndex] + " => ");
            byte gradesAmountForStudent = (byte)(Math.random() * (9 - 5) + 5);
            grades[studentIndex] = new byte[gradesAmountForStudent];
            for (int gradesIterator = 0; gradesIterator < gradesAmountForStudent; gradesIterator++) {
                grades[studentIndex][gradesIterator] = (byte) (Math.random() * (11 - 4) + 4);
                System.out.print(" " + grades[studentIndex][gradesIterator]);
            }
            System.out.println();
        }
        for (int studentIndex = 0; studentIndex < students.length; studentIndex++) {
            System.out.print("Average for " + students[studentIndex] + " => ");
            double averageGrade = 0.0;
            for(byte grade: grades[studentIndex]) {
                averageGrade += grade;
            }
            averageGrade /= grades[studentIndex].length;
            System.out.println(averageGrade);
        }
    }
}
