package it.stefanobizzi.corsi.javase.arraysamples;

public class BreakContinueSample {
    public static void main(String[] args) {
        short[][] hayStack = {
                {99, 23, 91, 11, 91, 96},
                {87, 91, 22, 18, 19, 43}
        };
        searchElementWithContinueLabel(hayStack, (short)91);
    }

    public static void searchElementWithContinueLabel(short[][] hayStack, short needle) {
        external: for (int i = 0; i < hayStack.length; i++) {
            for (int x = 0; x < hayStack[i].length; x++) {
                System.out.println("Compare " + needle + " with " + hayStack[i][x]);
                if (needle == hayStack[i][x]) {
                    System.out.println("Found at [" + i + "][" + x + "]");
                    continue external;
                }
                System.out.println("Logic B");
            }
            System.out.println("Logic A");
        }
    }

    public static void searchElementWithContinue(short[][] hayStack, short needle) {
        for (int i = 0; i < hayStack.length; i++) {
            for (int x = 0; x < hayStack[i].length; x++) {
                System.out.println("Compare " + needle + " with " + hayStack[i][x]);
                if (needle == hayStack[i][x]) {
                    System.out.println("Found at [" + i + "][" + x + "]");
                    continue;
                }
                System.out.println("Logic B");
            }
            System.out.println("Logic A");
        }
    }

    public static void searchElementWithBreakLabel(short[][] hayStack, short needle) {
        external: for (int i = 0; i < hayStack.length; i++) {
            for (int x = 0; x < hayStack[i].length; x++) {
                System.out.println("Compare " + needle + " with " + hayStack[i][x]);
                if (needle == hayStack[i][x]) {
                    System.out.println("Found at [" + i + "][" + x + "]");
                    break external;
                }
                System.out.println("Logic B");
            }
            System.out.println("Logic A");
        }
    }

    public static void searchElementWithBreak(short[][] hayStack, short needle) {
        for (int i = 0; i < hayStack.length; i++) {
            for (int x = 0; x < hayStack[i].length; x++) {
                System.out.println("Compare " + needle + " with " + hayStack[i][x]);
                if (needle == hayStack[i][x]) {
                    System.out.println("Found at [" + i + "][" + x + "]");
                    break;
                }
                System.out.println("Logic B");
            }
            System.out.println("Logic A");
        }
    }

    public static void searchElement(short[][] hayStack, short needle) {
        for (int i = 0; i < hayStack.length; i++) {
            for (int x = 0; x < hayStack[i].length; x++) {
                System.out.println("Compare " + needle + " with " + hayStack[i][x]);
                if (needle == hayStack[i][x]) {
                    System.out.println("Found at [" + i + "][" + x + "]");
                }
                System.out.println("Logic B");
            }
            System.out.println("Logic A");
        }
    }
}















