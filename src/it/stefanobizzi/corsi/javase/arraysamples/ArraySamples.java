package it.stefanobizzi.corsi.javase.arraysamples;

public class ArraySamples {
    public static void main(String[] args) {
        byte[] myFirstArray = {4, 9, 7, 2, 1};
        byte[] mySecondArray;
        mySecondArray = new byte[] { 4, 9, 1, 8, 13};
        byte[] myThirdArray = new byte[5];
        System.out.println(myFirstArray[2]);
        System.out.println("MyThirdArray[0]" + myThirdArray[0]);
        myThirdArray[0] = 34;
        System.out.println("MyThirdArray[0]" + myThirdArray[0]);

        byte[][] myFourthArray = new byte[3][];
        myFourthArray[0] = new byte[] { 1, 3};
        myFourthArray[1] = new byte[] { 4, 9, 8};
        myFourthArray[2] = new byte[] { 87, 65, 32, 12, 8};
        System.out.println("myFourthArray[1][2] = " + myFourthArray[1][2]);

        System.out.println("------------------");
        System.out.println("MyFirstArray => ");
        for(int i = 0; i < myFirstArray.length; i++) {
            System.out.print(" " + myFirstArray[i]);
        }
        System.out.println("\r\n------------------");

        int x = 0;
        while (x < myFourthArray.length) {
            System.out.println("Row [" + x + "] => ");
            for(byte element: myFourthArray[x]) {
                System.out.print(" " + element);
            }
            System.out.println("");
            x++;
        }



















    }
}
