package it.stefanobizzi.corsi.javase.arraysamples;

import java.util.Arrays;

public class SplitSample {
    public static void main(String[] args) {
        String example = "Il gatto salta sul tavolo";
        String[] exampleParts = example.split(" ");
        System.out.println(Arrays.toString(exampleParts));
    }
}
