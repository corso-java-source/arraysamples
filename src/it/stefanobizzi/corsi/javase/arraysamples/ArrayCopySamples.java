package it.stefanobizzi.corsi.javase.arraysamples;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayCopySamples {
    public static void main(String[] args) {
        int[] first = {12, 43, 86, 99, 18};
        int[] second = {65, 44, 25, 98, 90};
        System.out.println(Arrays.toString(first));
        int[] firstCopy = Arrays.copyOf(first, 15);
        System.out.println(Arrays.toString(firstCopy));
        int[] firstPartial = Arrays.copyOfRange(first, 3, 18);
        System.out.println(Arrays.toString(firstPartial));

        first = Arrays.copyOf(first, 15);
        System.out.println(Arrays.toString(first));
        System.arraycopy(second, 1, first, 10, 3);
        System.out.println(Arrays.toString(first));
        Arrays.fill(first, 5, 10, 8);
        System.out.println(Arrays.toString(first));
    }
}
