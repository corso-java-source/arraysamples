package it.stefanobizzi.corsi.javase.arraysamples;

import java.util.Arrays;

public class ArrayCopyChallenge {
    public static void main(String[] args) {
        String name = "MARIO;ROSSI";
        String address = "VIA VERDI 1;MILANO";
        String[] nameParts = name.split(";");
        String[] addressParts = address.split(";");
        String[] data = Arrays.copyOf(nameParts, 5);
        System.out.println(Arrays.toString(data));
        data[2] = "C";
        System.out.println(Arrays.toString(data));
        System.arraycopy(addressParts, 0, data, 3, 2);
        System.out.println(Arrays.toString(data));
        String[] additionalData = Arrays.copyOfRange(data, 2, 10);
        System.out.println(Arrays.toString(additionalData));
        Arrays.fill(additionalData, 3, 6, "X");
        System.out.println(Arrays.toString(additionalData));
        System.arraycopy(nameParts, 0, additionalData, 6, 2);
        System.out.println(Arrays.toString(additionalData));
    }
}
