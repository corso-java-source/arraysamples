package it.stefanobizzi.corsi.javase.arraysamples;

import java.util.Arrays;

public class VarargsSample {
    public static void main(String[] args) {
        String[] sortedElements = orderElements("Casa", "Automobile", "Barile", "Albero");
        String[] unsortedElements = orderElements(false, "Casa", "Automobile", "Barile", "Albero");
//        String[] emptyList = orderElements();
        System.out.println(Arrays.toString(sortedElements));
        System.out.println(Arrays.toString(unsortedElements));
//        System.out.println(Arrays.toString(emptyList));
        int[] sortedInts = orderElements(98, 54, 23, 1);
        System.out.println(Arrays.toString(sortedInts));

        String[] myArray = {"Casa", "Automobile", "Barile", "Albero"};
        orderElements(myArray);
    }

    public static String[] orderElements(String... elements) {
        return orderElements(true, elements);
    }

    public static String[] orderElements(boolean order, String... elements) {
        if (order) {
            Arrays.sort(elements);
        }
        return elements;
    }

    public static int[] orderElements(int... elements) {
        Arrays.sort(elements);
        return elements;
    }
}
